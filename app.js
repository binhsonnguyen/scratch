'use strict'

let http = require('http')

let app = http.createServer((req, res) => {
  let requestedURL = req.url
  console.log('got a request for ' + req.url + ' from ' + req.headers['user-agent'])
  res.writeHead(200, {'Content-Type': 'text/plain'})
  res.end('feature-01')
})

const PORT = 8000
app.listen(PORT)
console.log('feature 01 running at ' + PORT)

